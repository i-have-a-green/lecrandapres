<?php
/**
 * Displays header site branding
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

$blog_info    = get_bloginfo( 'name' );
$description  = get_bloginfo( 'description', 'display' );
$show_title   = ( true === get_theme_mod( 'display_title_and_tagline', true ) );
$header_class = $show_title ? 'site-title' : 'screen-reader-text';

?>



<div class="site-branding">

	<?php if ( has_custom_logo()) : ?>
		<div class="site-logo">
			<a href="<?php echo home_url();?>" title="<?php _e("Back to home", "ihag");?>">
				<?php the_custom_logo(); ?>
			</a>
		</div>
	<?php endif; ?>

	
</div><!-- .site-branding -->
